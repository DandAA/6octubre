<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>


<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('alumnos')?>" method="post">
            <label for="texto" class="text-success">Introduce los Datos </label>
            <br>
            <input type="text" name="NIA" value="Pon un NIA" id="texto" />
            <br>
            <input type="text" name="id" value="Pon un id" id="texto" />
            <br>
            <input type="text" name="nif" value="Pon un nif" id="texto" />
            <br>
            <input type="text" name="nombre" value="Pon un Nombre" id="texto" />
            <br>
            <input type="text" name="apellido1" value="Pon el 1er Apellido " id="texto" />
            <br>
            <input type="text" name="apellido2" value="Pon el 2er Apellido" id="texto" />
            <br>
            <input type="text" name="email" value="Pon un Email" id="texto" />
            <br>
            <input type="submit" name="enviar" value="Enviar" />
        </form>
    </body>
</html>
