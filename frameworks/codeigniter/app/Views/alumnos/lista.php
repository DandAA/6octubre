<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>


<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?=$title;?></title>
    </head>
    <body>
         <h1 class="text-primary"><?= "Listado de Alumnos";?></h1>
         <!-- <h2>ID&nbsp&nbsp&nbsp&nbsp&nbsp NIA&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Nif&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp FecNac&nbsp Nombre&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Apellido1&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Apellido2&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Email </h2>-->
        <table class="table table-striped">
             <?php foreach ($alumnos as $alumno): ?>
                <tr>
                    <td>
             
                        <?= $alumno-> id ?> 
                    </td>
                     <td>
             
                        <?= $alumno-> NIA ?> 
                    </td>
                     <td>
             
                        <?= $alumno-> nif ?> 
                    </td>
                    <td>
             
                        <?= $alumno-> fecha_nac ?> 
                    </td>
                    <td>
                        <?= $alumno-> nombre ?> 
                    </td>
                    <td>
                        <?= $alumno-> apellido1 ?>
                    </td>
                     <td>
             
                        <?= $alumno-> apellido2 ?> 
                    </td>
                    <td>
             
                        <?= $alumno-> email ?> 
                        <a class="btn btn-primary" href=<?=site_url('alumnos/editar/',$alumno->id)?> role="button">Editar</a>
                        
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </body>
</html>
