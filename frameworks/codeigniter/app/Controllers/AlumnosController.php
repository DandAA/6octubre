<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\Alumnos;

/**
 * Description of AlumnosController
 *
 * @author a0y6443291q
 */
class AlumnosController extends BaseController
{

    //put your code here
    
    public function listaAlumnos()
    {
        $data['title'] = "Listado de Grupos";
        $datos = new Alumnos();
        //$textobuscar= $this ->request->getPost('texto');
        //$data['alumnos'] = $datos->like(['nombre'=> strtoupper($textobuscar)])->findAll();
       
      
        
        $data['alumnos'] = $datos->findAll();
      
        return view('alumnos/lista',$data);
    }
    
    
    
     public function  formAlumnos()
    {
    
        $data['title'] = "Formulario de Entrada";
     
        return view('alumnos/form',$data);
    }
    
    
        
         public function  formBuscar()
    {
    
        $data['title'] = "Formulario de Entrada";
     
        return view('alumnos/formbuscar',$data);
  
        
    }

    
    public function agregarAlumnos() 
    {
        
        $data['title'] = "Insertar Alumno";
        $datos = new \App\Models\Alumnos();
        

        
        $nia = $this ->request->getPost('NIA');
        $id = $this ->request->getPost('id');
        $nif = $this ->request->getPost('nif');
        $nombre = $this ->request->getPost('nombre');
        $apellido1 = $this ->request->getPost('apellido1');
        $apellido2 = $this ->request->getPost('apellido2');
        $email = $this ->request->getPost('email');
        //conexion
        $alumno = ["NIA" => $nia,
                    "id" => $id,
                    "nif" => $nif,
                    "nombre" => $nombre,
                    "apellido1" => $apellido1,
                    "apellido2" => $apellido2,
                    "email" => $email];
        
        /*echo '<pre>';
        print_r($alumno);
        echo '</pre>';*/
        
        $datos->insert($alumno);
        //return view('alumnos/form',$data);
        //return redirect()->to('/listaAlumnos');
                return redirect()->to('/formBuscar');

    }
    
    public function form() {
        helper('update');
        $data['titulo'] ='Lista de Alumnos';
        return view('alumnos/update');
    }
    
    public function editar($id) {
        helper('formEdit');
        $alumnoModel = new Alumnos();
        $data['titulo']='Editar Alumnos';
        $data['alumno']=$alumnoModel->find($id);
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        //return redirect()->to('alumnos/update');
        
    }
} 
     
    