<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Alumnos
 *
 * @author a0y6443291q
 */
namespace App\Models;
use CodeIgniter\Model;

class Alumnos extends Model {
    protected $table='alumnos';
    protected $primaryKey='id';
    protected $returnType = 'object';
    protected $allowedFields = ["nif",'NIA','id', 'nombre', 'apellido1','apellido2','email',];

    
}
